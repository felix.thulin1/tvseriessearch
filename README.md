# Tv-series-search Felix T Squeed

Programming test to create a web-application using tvmaze API.

## To run

For this project, have node package manager installed.

Run:
### `npm install`

Installs necessary packages.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.


Schedule-fetch (home page) uses American ISO as it generates quantitative data

Small fixes that got excluded due to time:
    - Fundamental buttons that would make navigation easier 
    - Prevent flicker from img when its loading /or use spinner
    - Show episodelist on SeriePage
    - Use a different state-structure
    - Improve component-tree