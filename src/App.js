import React, { useState, useEffect } from "react";
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'
import ChannelItems from './components/ChannelItems';
import Header from './components/Header'
import SearchResultItems from './components/SearchResultItems'
import SeriePage from "./components/SeriePage";

const App = () => {
  const [scheduleList, popSeriesList] = useState([])
  const [searchList, popSearchList] = useState([])

  // Group list by channels
  const groupList = async (arr) => {
    var list = arr
    let result = list.reduce((r, a) => {
      try {
        if (!a.show.network || !a.show) {
          a.show.network = {name: 'web'}
        }
        
        if (a.show.network && a.show) {
          r[a.show.network.name] = r[a.show.network.name] || [];
          r[a.show.network.name].push(a);
          return r;
        }
      } catch (e) {
        console.log ('err', e)
      }
    }, Object.create(null));
    result = Object.entries(result)
    return result
  }

  // Fetch scheduled series from api
  const fetchScheduleList = async () => {
    const res = await fetch('https://api.tvmaze.com/schedule?country=US&date=') 
    const data = await res.json()
    return data
  }

  // Search series from api
  const searchSerieList = async (value) => {
    if (value === '') {
      return popSearchList([])
    }
    const res = await fetch('https://api.tvmaze.com/search/shows?q=:' + value) 
    const data = await res.json()
    popSearchList(data)
    return 
  }

  useEffect (() => {
    const getSeriesList = async () => {
      const fetchedSeriesList = await fetchScheduleList()
      const sorted = await groupList(fetchedSeriesList)
      popSeriesList(sorted)
    }
    getSeriesList()
  }, [])

  return (
    <Router>
      <div className="main-container">
        <Header onSearch={searchSerieList} />
          <Routes>
            <Route path="/" exact element={
              <>
                {searchList.length > 0 ? (
                  <div className="row-container">
                    <SearchResultItems searchResult={searchList} />
                  </div>
                ) : (
                  <div className="grid-container">
                    {scheduleList.map((channel, index) => (
                      <ChannelItems key={index} channel={channel[0]} series={channel[1]}/>
                    ))}
                  </div>
                  )}
              </>
            } />
            <Route path="/seriePage" element={<SeriePage />} />
          </Routes>
        </div>
    </Router>
  );
}

export default App;
