const SerieScheduleItem = ({serie}) => {
  return (
    <div className="serie-schedule-container">
        <p className="serie-airtime">{serie.airtime}</p>
        <p className="serie-title">{serie.show.name}</p>
    </div>
  )
}

export default SerieScheduleItem