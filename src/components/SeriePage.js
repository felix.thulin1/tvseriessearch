import { useState, useEffect } from "react";
import { useSearchParams } from "react-router-dom"
import SeriePageListPresentation from "./serie-page-components/SeriePageListPresentation";

const SeriePage = () => {
    const [searchParams] = useSearchParams()
    const [serieData, setSerieData] = useState({})
    const serieId = searchParams.get('id')

    // Fetch the serie-object from api
    const fetchSerieData = async (serieId) => {
        const string = 'https://api.tvmaze.com/shows/' + serieId
        const res = await fetch(string) 
        const data = await res.json()
        return data
    }

    // Get year the serie premiered (e.g '2012')
    const getPremiereYear = (dateString) => {
        const year = new Date(dateString).getFullYear() || ''
        return year
    }

    // Removes <tags> from sring
    const handleSummaryString = (summaryString) => {
        const regex = /(<([^>]+)>)/ig
        return summaryString?.replace(regex, '')
    }

    // Get IMDB information such as rating and link to IMDB-page (e.g {rating: '9.4', link: "https://www.imdb.com/title/tt7366338/"})
    const getImdb = () => {
        console.log('imdb', serieData)
        let imdb = {}
        imdb.link = serieData.externals?.imdb ? ("https://www.imdb.com/title/" + serieData.externals.imdb + "/") : ('')
        imdb.rating = serieData.rating?.average || '--'
        return imdb
    }

    // Get the genres as a string (e.g 'Drama, Thriller')
    const getGenres = () => {
        let string = ''
        serieData.genres?.forEach(element => {
            string.length < 1 ? (string = element) : (string += (', ' + element))
        });
        return string
    }

    // Sets the data that is sent to <SeriePageListPresentation />
    const setPresentation = () => {
        const data = {}
        if (!serieData) {
            return
        }
        data.type = serieData.type || ''
        data.runtime = serieData.runtime ? (serieData.runtime + ' min') : ('')
        data.premiereYear = getPremiereYear(serieData.premiered)
        data.genres = getGenres()
        data.imdb = getImdb()
        return data
    }

    useEffect (() => {
        const getData = async () => {
            if (serieId){
                const serie = await fetchSerieData(serieId)
                setSerieData(serie)
            }
        }
        getData()
    }, [])

  return (
    <div className="serie-page-container">
        {serieData.image != null ? (
            <img className="serie-page-image" alt="Not availabe" src={serieData.image.original} />
        ) : (<div className="serie-page-image" />)}

            <div className="serie-page-name-container">
                <h1>{serieData.name}</h1>
                <SeriePageListPresentation serieData={setPresentation()} />
            </div>
            <div className="serie-page-summary-container">
                <p>{handleSummaryString(serieData.summary)}</p>
            </div>
    </div>
  )
}

export default SeriePage