import {Link} from 'react-router-dom'
import SerieSearchItem from './SerieSearchItem'
import SerieScheduleItem from './SerieScheduleItem'

const SerieItems = ({searchResult, series}) => {
  const isSearchResult = searchResult || false
  // isSearchResult dictates if SerieItems should be presented as search-items or schedule-items
  
  return (
    <div>  
      {series.map((serie, index) =>(
        <Link key={index} className='item-link' to={{
          pathname: '/seriePage',
          search: '?id=' + serie.show.id
        }}>
          {isSearchResult ? (<SerieSearchItem key={index} serie={serie} />) : (<SerieScheduleItem key={index} serie={serie} />)}
        </Link>
      ))}
    </div>
  )
}

export default SerieItems