import {FaStar} from 'react-icons/fa'

const SeriePageListPresentation = ({serieData}) => {
    /*  If presentation-attribute doesnt have value it is not shown - except imdb-score which will 
        have a '--' value to still make the series imdb-page accessible by clicking on it.
    */
    console.log('seriedata', serieData)
    return (
        <div className='serie-page-presentation-container'>
            {serieData.type && <p>{serieData.type}</p>}
            {serieData.runtime && <p>{serieData.runtime}</p>}
            {serieData.premiereYear && <p>{serieData.premiereYear}</p>}
            {serieData.genres && <p>{serieData.genres}</p>}
            {serieData.imdb.link && <a href={serieData.imdb.link} target="_blank" rel='noreferrer' >
                <p>{serieData.imdb.rating}<FaStar style={{color: '#F5C518', height: '13px'}}  /></p>
            </a>}
        </div>
    )
}

export default SeriePageListPresentation