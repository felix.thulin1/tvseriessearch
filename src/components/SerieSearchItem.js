
const SerieSearchItem = ({serie}) => {
  return (
        <div className="serie-search-container">
            {serie.show.image != null ? (
                <img className="serie-search-image" alt="Not availabe" src={serie.show.image.medium} />
            ) : (<div className="serie-search-image-placeholder" />)}
            <h2 className="serie-search-title">{serie.show.name}</h2>
        </div>
    )
}

export default SerieSearchItem