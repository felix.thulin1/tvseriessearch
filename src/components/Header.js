import {useNavigate } from 'react-router-dom'
import React, { useState } from "react";
import {FaSearch, FaTimes} from 'react-icons/fa'

const Header = ({onSearch}) => {
    const [text, setText] = useState('')
    let navigate = useNavigate()
    function handleString(string){
        setText(string)
        // If searchbox is empty, return to default content
        if (string === '') {
            onSearch(string)
        }
    }

    return (
        <div className='header'>
            <div className='search-container'>
                <form className="search-form-container" autoComplete="off" onSubmit={(e) => {e.preventDefault(); onSearch(text); navigate('/')}} >
                    <input placeholder="Search.." className='search-field' value={text} onChange={(e) => handleString(e.target.value)}></input>
                    {text && <FaTimes className='search-cancel-button' onClick={(e) => handleString('')} />}
                    <button className='search-button' type="submit">
                        <FaSearch  />
                    </button>
                </form>
            </div>
        </div>
    )
}

export default Header