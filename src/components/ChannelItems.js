import SerieItems from './SerieItems'

const ChannelItems = ({channel, series}) => {
  return (
    <div className='channel-container'>
        <h2 className='channel-title'>{channel}</h2>
        <SerieItems series={series} />
    </div>
  )
}

export default ChannelItems