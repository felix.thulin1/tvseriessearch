
import SerieItems from "./SerieItems"

const SearchResultItems = ({searchResult}) => {
  return (
    <div>
        <SerieItems searchResult={true} series={searchResult} />
    </div>
  )
}

export default SearchResultItems